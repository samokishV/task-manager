<?php

namespace App\Controllers;

use App\Lib\View as View;
use App\Lib\Route as Route;
use App\Models\User as User;
use App\Models\Tasks as Task;
use App\Lib\Pagination as Pagination;
use App\Lib\Controller as Controller;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->model = new Task();
        $this->view = new View();
        $this->pagination = new Pagination();
		$this->route = new Route();
		$this->route->start();
    }

	public function index()
	{
        $params = $this->route->getParams();

        $page = $params['page'] ?? 1;
        $order = $params['order'] ?? 'created_at-desc';
        
        $data = $this->model->getList($page, $order);
        $total = $this->model->countTotal();

        $pagination = $this->pagination->generate($total, $page, $order);

        if(User::isAdmin()) {
            $mainViewName = 'main_admin_view.html';
        } else {
            $mainViewName = 'main_view.html';
        }

		$this->view->generate($mainViewName, 'template_view.html', $data, $pagination, $page);
	}

    public function show()
    {
		$url = $this->route->getParams();
        $data = $this->model->getByUrl($url);
        $this->view->generate('page_view.html', 'template_view.html', $data);
    }

    public function create() 
    {
        $this->view->generate('add_view.html', 'template_view.html');
    }

    public function add() {
        $name = htmlentities($_POST['name']) ?? '';
        $email = htmlentities($_POST['email']) ?? '';
        $text = htmlentities($_POST['text']) ?? '';

        $this->model->add($name, $email, $text);

        return json_encode(['success' => true]);
    }

    public function edit() {
        if(!User::isAdmin()) {
            http_response_code('401');
            return;
        }

        $path = $this->route->getUri();
        $id = basename($path);

        if(isset($_POST['status'])) {
            $this->model->editStatus($id, $_POST['status']);
        }
        
        if(isset($_POST['text'])) {
            $this->model->editText($id, htmlentities($_POST['text']));
        }
    }
}
