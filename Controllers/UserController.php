<?php

namespace App\Controllers;

use App\Lib\Controller as Controller;
use App\Lib\View as View;
use App\Lib\Authorization as Authorization;
use App\Lib\Route as Route;
use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->view = new View();
        $this->model = new User();
    }

    public function login()
    {
        if ($_POST && isset($_POST['email']) && isset($_POST['password'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            Authorization::login($email, $password);
        }

        $this->view->generate('auth_view.html', 'template_view.html');
    }
    
    public function logout()
    {
        Authorization::logout();
        header("Location:".PRE);
    }
}
