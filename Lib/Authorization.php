<?php

namespace App\Lib;

use App\Models\User as User;
use App\Lib\Cookie as Cookie;
use App\Lib\Session as Session;

class Authorization
{
    public function login($login, $password)
    {
        if (isset($login) && isset($password)) {
            if (!Validation::loginExists($login)) {
                Session::setFlash('Incorrect login. Please try again.', 'danger');
                return false;
            } else {
                $user = new User();
                $user = $user->getByLogin($login);
                if($user) {
                    $password = $user->password;
                    $login = $user->login;
                }

                if ($password == hash('md5', $_POST['password'])) {
                    Session::set('login', $login);
                    Cookie::set('login', $login);
                    Session::setFlash('Authorization completed successfully.', 'success');
                    return true;
                } else {
                    Session::setFlash('Incorrect password. Please try again.', 'danger');
                    return false;
                }
            }
        }
    }

    public static function isAuth()
    {
        if (Session::get('login')) {
            return true;
        } 

        if (Cookie::get('login')) {
            $login = Cookie::get('login');
            Session::set('login', $login);
            return true;
        }   
    }

    public function logout()
    {
        if (self::isAuth()) {
            Session::delete('login');
            Cookie::delete('login');
        }
    }
}
