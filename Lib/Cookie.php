<?php

namespace App\Lib;

class Cookie
{
    public static function set($key, $value, $time = 60 * 60 * 24 * 7)
    {
        setcookie($key, $value, time() + $time, '/') ;
    }

    public static function get($key)
    {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }
        return null;
    }

    public static function delete($key)
    {
        foreach($_COOKIE as $name => $cookie) {         
            if($name==$key) {
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
                return true;
            }
        }
    }

    public static function deleteAll()
    {
        foreach($_COOKIE as $name => $cookie) {         
            setcookie($name, '', time()-1000);
            setcookie($name, '', time()-1000, '/');
            return true;
        }
    }
}
