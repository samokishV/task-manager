<?php

namespace App\Lib;

class DB  
{
    protected static $pdo;

    public static function connect()
    {
        $config = include('config.php');
        extract($config);

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
        \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        self::$pdo = new \PDO($dsn, $user, $pass, $opt);
        return self::$pdo;
    }
} 
