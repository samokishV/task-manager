<?php

namespace App\Lib;

class Pagination
{
    public const LIMIT_DEFAULT = 3;

    public function generate($total, $activePage, $order)
    {
        $pagesTotal = self::countPages($total);
        ob_start();
        require_once 'views/pagination.html';
        return ob_get_clean();
    }

    public static function getOffset($page) 
    {
        $numberTasksPerPage = self::LIMIT_DEFAULT;
        $offset = ($page - 1) * $numberTasksPerPage;
        return $offset;
    }

    public static function getLimit() 
    {
        return self::LIMIT_DEFAULT;
    }

    public static function countPages($total) 
    {
        return ceil($total/self::LIMIT_DEFAULT);
    }
}