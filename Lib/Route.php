<?php

namespace App\Lib;

use App\Models\Tasks as Task;

class Route
{
    protected $controller="Task";
    protected $action="index";
    protected $params;

    public function getController()
    {
        return $this->controller."Controller";
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function start()
    {
        $uri = self::getUri();
        $method = $_SERVER['REQUEST_METHOD'];

        if($uri == '/' && $method == "GET") {
            $this->controller = "Task";
            $this->action = "index";
            $this->params = self::getUriParams();
        }

        if($uri == '/create' && $method == "GET") {
            $this->controller = "Task";
            $this->action = "create";
        }

        if($uri == '/api/task' && $method == "POST") {
            $this->controller = "Task";
            $this->action = "add";
        }

        if(preg_match('/^\/api\/task\/[\d]+$/', $uri) && $method == "POST") {
            $this->controller = "Task";
            $this->action = "edit";
        }

        if($uri == '/login') {
            $this->controller = "User";
            $this->action = "login";
        }

        if($uri == '/logout') {
            $this->controller = "User";
            $this->action = "logout";
        }
    }

    public static function getUri() {
        $uri = str_replace(PRE, '', $_SERVER['REQUEST_URI']);
        $url_components = parse_url($uri); 
        return $url_components['path'] ?? '';
    }

    public static function getUriParams() {
        $uri = str_replace(PRE, '', $_SERVER['REQUEST_URI']);
        $url_components = parse_url($uri); 

        if(!isset($url_components['query'])) {
            return;
        }

        $paramsArray = explode('&', $url_components['query']);

        $params = [];

        foreach($paramsArray as $param) {
            $tempArr = explode('=', $param);
            $params[$tempArr[0]] = $tempArr[1];
        }

        return $params;
    }
}
