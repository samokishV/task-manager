<?php
namespace App\Lib;

class View
{
    public function generate($contentView, $templateView, $data = null, $pagination = null, $activePage = null)
    {
        require_once 'views/'.$templateView;
    }
}
