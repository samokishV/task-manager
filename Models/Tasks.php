<?php

namespace App\Models;

use App\Lib\DB as DB;
use App\Lib\Route as Route;
use App\Lib\Pagination as Pagination;
use App\Lib\Validation as Validation;

class Tasks
{
    public function __construct() 
    {
	    $this->pdo = DB::connect();
    }

    public function add($name, $email, $text)
    {
        $stmt= $this->pdo->prepare("INSERT INTO tasks (user_name, email, text) VALUES(:user_name, :email, :text)");
        $stmt->execute(array('user_name'=>$name, 'email'=>$email, 'text'=>$text));
        return true; 
    }

    public function editStatus($id, $status)
    {
        $status = (int) filter_var($status, FILTER_VALIDATE_BOOLEAN);

        $stmt = $this->pdo->prepare("UPDATE tasks SET status=:status WHERE id=:id");
        $stmt->execute(array('id'=>$id, 'status'=>$status)); 
        return true;
    } 

    public function editText($id, $text)
    {
        $edited = self::TextModified($id, $text);
        var_dump($edited);
        $stmt= $this->pdo->prepare("UPDATE tasks SET text=:text, edited=:edited WHERE id=:id");
        $stmt->execute(array('id'=>$id, 'text'=>$text, 'edited'=>$edited)); 
        return true;
    } 

    public static function TextModified($id, $text) 
    {
        $task = new Tasks();
        $data = $task->getById($id);

        if($data->text !== $text) {
            return true;
        }
    }

    public function getById($id)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM tasks WHERE id=:id');
        $stmt->execute(array('id'=>$id));
        $stmt->setFetchMode(\PDO::FETCH_CLASS, get_class($this));
        $pages = $stmt->fetch(\PDO::FETCH_CLASS);
        return $pages;
    }

    public function getList($page, $order)
    {
        $limit = Pagination::getLimit();
        $offset = Pagination::getOffset($page);

        $orderParts = explode('-', $order);
        $column = $orderParts[0];
        $direction = $orderParts[1];

        $stmt = $this->pdo->query("SELECT * FROM tasks order by $column $direction limit $limit offset $offset");
        $stmt->setFetchMode(\PDO::FETCH_CLASS, get_class($this));
        $tasks= $stmt->fetchAll(\PDO::FETCH_CLASS);
        return $tasks;
    }

    public function countTotal() 
    {
        $stmt = $this->pdo->query("SELECT count(*) as totalCount FROM tasks");
        $result= $stmt->fetch();
        return $result->totalCount;
    }
}
