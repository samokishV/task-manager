<?php

namespace App\Models;

use App\Lib\DB as DB;
use App\Lib\Session as Session;
use App\Lib\Validation as Validation;

class User
{
	public function __construct() 
	{
		$this->pdo = DB::connect();
	}

    public static function isAdmin()
    {
        $login = Session::get('login');
        if($login) {
            $user = (new User())->getByLogin($login);
            if(isset($user->role) && $user->role = 'admin') {
                return true;
            }
        }
    }

    public function getByLogin($login)
    {
        $stmt = $this->pdo->prepare('SELECT * from users WHERE login=:login');
        $stmt->execute(array('login' => $login));
        $stmt->setFetchMode(\PDO::FETCH_CLASS, get_class($this));
        $object = $stmt->fetch(\PDO::FETCH_CLASS);
        return $object;    
    }
}
