<?php

use App\Lib\Route as Route;

session_start();

ini_set('display_errors', 1);

define('DIR', str_replace("\\", '/', __DIR__));
require DIR."/init.php" ;
$root = $_SERVER['DOCUMENT_ROOT'];
$prefix = str_replace($root, '', DIR);
define('PRE', $prefix);

$route = new Route();
$route->start();

$action = $route->getAction();
$controller = "\App\Controllers\\".$route->getController();
$controller1 = new $controller();
$controller1->$action();
