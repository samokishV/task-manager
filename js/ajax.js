/**
 *
 * @param {string} type
 * @param {string} url
 * @param {object} data
 * @param successCallback
 * @returns {*|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType, statusCode}}
 */
function request(type, url, data, successCallback) {
    return $.ajax({
        type,
        url,
        data,
        success(data) {
            successCallback(data);
        },
        error(req, status, err) {
            alert('something went wrong', status, err);
        },
    });
}
  