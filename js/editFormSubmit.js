$(document).ready(() => {
    $('.edit-form').submit(function (e) {
        e.preventDefault();
        const data = $(this).serialize();
        const fields = $(this).serializeArray();
        let id;

        jQuery.each( fields, function( i, field ) {
            if (field.name == 'id') {
                id = field.value;
            }
        });

        const type = 'POST';
        const href = "api/task/" + id;

        request(type, href, data, (result) => {
        });
    
        return false;
    });
});