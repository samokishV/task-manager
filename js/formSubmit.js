$(document).ready(() => {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            event.preventDefault();

            const name = $("#name").val();
            const email = $("#email").val();
            const text = $("#text").val();
           
            const type = 'POST';
            const href = "api/task";
            const data = {name: name, email:email, text: text};
        
            request(type, href, data, (result) => {
                $('#status-message').html("Task add successfully");
                $('#status-message').addClass('alert-success');
            });

            return false;
        }
        form.classList.add('was-validated');
    }, false);
    });
});