$(document).ready(function() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const order = urlParams.get('order');

    $("#order").val(order);

    $('#order').on('change', function() {
        $('#order-form').submit();
    });
});