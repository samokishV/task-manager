$(document).ready(() => {
    const checkbox = $("input[type=checkbox]");

    checkbox.change(function(event) {
        const checkbox = event.target;
        const status = checkbox.checked;
        const id = checkbox.getAttribute('value');
       
        const type = 'POST';
        const href = "api/task/" + id;
        const data = {status: status};
    
        request(type, href, data, () => {
        });
    });
}); 